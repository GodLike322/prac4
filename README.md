# prac4
# Создание программы "счастливый билет"

## Задание

Дано некоторое целое положительное шестизначное число , не содержащее нулей, являющееся номером билета. Назовём номер «счастливым», если сумма чисел, стоящих на чётных позициях, будет равна сумме чисел, стоящих на нечётных позициях. Необходимо вывести два других «счастливых» билета – предыдущий и последующий за . 

## Этапы

1. **Написана программа согласно заданию из 4 практической.**
    * *Написана функция проверки ввода*
        ```python
                def main():
                    ticket = input('Введите Билет: ')
                    while type(ticket) == str:
                        try:
                            int(ticket)
                            if '0' in ticket:
                                print('Вы ввели 0(UNACCEPTABLE)')
                                ticket = input()
                            elif int(ticket) <= 999999 and int(ticket) >= 111111:
                                break
                            else:

                                print('Вы ввели неправильное число, ')

                                ticket = input()
                        except ValueError:
                            print('Вы ввели не число')
                            ticket = input()
                    return ticket
            ```
        * 
        * *Написана функция выполняющая задание*
            ```python
    def check():
        ticket = main()
        Lucky = ''
        
        next_ticket = int(ticket) + 1
        next_ticket = str(next_ticket)
        previous_ticket = int(ticket) - 1
        previous_ticket = str(previous_ticket)
        
        if (int(ticket[1]) + int(ticket[3]) + int(ticket[5])) ==\
        (int(ticket[0]) + int(ticket[2]) + int(ticket[4])):
            print('Lucky ')

            count = 0
            while count == 0:
                if (int(next_ticket[1]) + int(next_ticket[3]) + int(next_ticket[5])) ==\
                (int(next_ticket[0]) + int(next_ticket[2]) + int(next_ticket[4])):
                    print(f'Следующее счастливое число - {next_ticket}')
                    count = 1
                else:
                    next_ticket = str(int(next_ticket) + 1)
                    
            count = 0
            while count == 0:
                if (int(previous_ticket[1]) + int(previous_ticket[3]) + int(previous_ticket[5])) ==\
                (int(previous_ticket[0]) + int(previous_ticket[2]) + int(previous_ticket[4])):
                    print(f'Предыдущее счастливое число - {previous_ticket}')
                    count = 1
                else:
                    previous_ticket = str(int(previous_ticket) - 1)
        else:
            print('Unlucky ')

            
    check()

       
2. **Написана документация** 

*Пример с функцией вып. задание:* 

```python
    """
    Вычисляет счастливый билет или нет.

    Принимает на вход билет.

    Raises:
        ValueError

    Examples:
        >>> lights_num(-15,2)
        Traceback (most recent call last):
            ...
        ValueError
    """
```
3. **Долго мучаемся с Doxygen**

![](http://memesmix.net/media/created/c0tzan.jpg)

4. **Редактируем README.md воспользовавшись [помощью](https://vk.com/verkhoturov2014)**

5. **Готово!**
